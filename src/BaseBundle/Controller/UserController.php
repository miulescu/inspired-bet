<?php

namespace BaseBundle\Controller;

use BaseBundle\Form\RegisterType;
use BaseBundle\Service\BaseService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package BaseBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @Route("{_locale}/register",
     *     name="base_user_register",
     *     requirements={"_locale":"%available_locales_requirement%"},
     *     defaults={"_locale":"%locale%"})
     *
     * @param Request $request
     *
     * @return null|Response
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(RegisterType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            return $this->get(BaseService::ID)->saveAndAuthenticateNewUser($form->getData(), $request);
        }

        return $this->render(
            '@Base/User/register.html.twig',
            array('registerForm' => $form->createView())
        );
    }

    /**
     * @Route("/login", name="base_user_login")
     */
    public function loginAction()
    {
    }

    /**
     * @Route("/logout", name="base_user_logout")
     */
    public function logoutAction()
    {
    }
}
