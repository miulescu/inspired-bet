<?php

namespace BaseBundle\Controller;

use GameBundle\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class BaseController
 * @package BaseBundle\Controller
 */
class BaseController extends Controller
{
    /**
     * @Route("/", name="base_base_setLocale")
     *
     * @return RedirectResponse
     */
    public function setLocaleAction()
    {
        return $this->redirectToRoute('base_base_index');
    }

    /**
     * @Route("/{_locale}/",
     *      name ="base_base_index",
     *      requirements={"_locale":"%available_locales_requirement%"},
     *      defaults={"_locale":"%locale%"})
     *
     * @return Response
     */
    public function indexAction()
    {
        /** @var GameService $gameService */
        $gameService = $this->get(GameService::ID);

        return $this->render('@Base/Base/index.html.twig', [
            'sportsCodes' => $gameService->getSportsCodes(),
        ]);
    }
}
