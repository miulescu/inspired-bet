<?php

namespace BaseBundle\Form;

use BaseBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'user.register.email',
                ]
            )
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'user.register.username',
                ]
            )
            ->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'first_options'  => array('label' => 'user.register.password'),
                    'second_options' => array('label' => 'user.register.repeatPassword'),
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'user.register.firstName',
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'user.register.lastName',
                ]
            )
            ->add(
                'budget',
                TextType::class,
                [
                    'label' => 'user.register.budget',
                    'mapped' => false,
                ]
            )
            ->add(
                'birthday',
                BirthdayType::class,
                [
                    'widget' => 'choice',
                    'label' => 'user.register.birthday',
                    'placeholder' => array(
                        'year' => 'user.register.year', 'month' => 'user.register.month', 'day' => 'user.register.day',
                    )
                ]
            )
            ->add(
                'country',
                CountryType::class,
                [
                    'label' => 'user.register.country',
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'label' => 'user.register.city',
                ]
            )
            ->add(
                'address',
                TextType::class,
                [
                    'label' => 'user.register.address',
                ]
            )
            ->add(
                'postalCode',
                TextType::class,
                [
                    'label' => 'user.register.postalCode',
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'label' => 'user.register.phone',
                ]
            )
            ->add(
                'nationalIdentificationNumber',
                TextType::class,
                [
                    'label' => 'user.register.nationalIdentificationNumber',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }
}