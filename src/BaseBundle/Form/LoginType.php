<?php

namespace BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class LoginType
 * @package BaseBundle\Form
 */
class LoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class,
                [
                    'attr' => [
                        'class' => 'input-sm',
                        'placeholder' => 'user.login.user',
                    ],
                ]
            )
            ->add('password', PasswordType::class,
                [
                    'attr' => [
                        'class' => 'input-sm',
                        'placeholder' => 'user.login.password',
                    ],
                ]
            );
    }
}
