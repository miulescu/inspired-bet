<?php


namespace BaseBundle\Security;


use BaseBundle\Entity\User;
use BaseBundle\Form\LoginType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class LoginFormAuthenticator
 * @package BaseBundle\Security
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    /** @var FormFactoryInterface  */
    private $formFactory;

    /** @var EntityManager  */
    private $em;

    /** @var RouterInterface  */
    private $router;

    /** @var UserPasswordEncoder  */
    private $passwordEncoder;

    /** @var  TranslatorInterface */
    private $translator;

    /**
     * LoginFormAuthenticator constructor.
     *
     * @param FormFactoryInterface $formFactory
     * @param EntityManager $em
     * @param RouterInterface $router
     * @param UserPasswordEncoder $passwordEncoder
     * @param TranslatorInterface $translator
     */
    public function __construct( FormFactoryInterface $formFactory, EntityManager $em, RouterInterface $router, UserPasswordEncoder $passwordEncoder, TranslatorInterface $translator)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }

    /**
     * @param Request $request
     *
     * @return mixed|void
     */
    public function getCredentials(Request $request)
    {
        $isLoginSubmit =
            $request->getPathInfo() == '/login'
            && $request->isMethod('POST');

        if (!$isLoginSubmit) {
            return;
        }

        $form = $this->formFactory->create(LoginType::class);
        $form->handleRequest($request);
        $data = $form->getData();

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['username']
        );

        return $data;
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return mixed
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['username'];

        return $this->em->getRepository(User::class)
            ->loadUserByUsername($username);
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['password'];

        return $this->passwordEncoder->isPasswordValid($user, $password);
    }

    /**
     * @return mixed
     */
    protected function getLoginUrl()
    {
        return $this->router->generate('base_base_index');
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->getSession() instanceof SessionInterface) {
            $message = $this->translator->trans($exception->getMessageKey(), $exception->getMessageData(), 'security');
            $request->getSession()->getFlashBag()->add(Security::AUTHENTICATION_ERROR, $message);
        }

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * Override to change what happens after successful authentication.
     *
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey
     *
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /** @todo redirect to index html if referer is /register */
        return new RedirectResponse($request->headers->get('referer'));
    }
}