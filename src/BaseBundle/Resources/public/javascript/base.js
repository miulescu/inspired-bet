function addNotification(message, type){
    return $.notify({
            icon: 'fa fa-exclamation-circle',
            message: message
        },{
        type: type,
        placement: {align: "center"},
        offset: {
            y: 100
        },
        icon_type: 'class'
    });
}
