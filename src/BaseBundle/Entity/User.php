<?php

namespace BaseBundle\Entity;

use BetBundle\Entity\BetSlip;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\UserBudget;
use UserBundle\Entity\UserTransaction;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="username", type="string", length=20, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="email", type="string", length=50, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="national_identification_number", type="string", length=50, nullable=true)
     */
    private $nationalIdentificationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="json_array")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="first_name", type="string", length=50)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=20)
     */
    private $lastName;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime")
     */
    private $birthday;

    /**
     * @var int
     *
     * @ORM\Column(name="country", type="string", length=5)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=50)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20)
     */
    private $phone;

    /**
     * @var int
     *
     * @ORM\Column(name="performance_factor", type="integer", nullable=true)
     */
    private $performanceFactor;

    /**
     * @var UserBudget
     *
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\UserBudget", mappedBy="user")
     */
    private $userBudget;

    /**
     * @var BetSlip[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BetBundle\Entity\BetSlip", mappedBy="user")
     */
    private $betSlips;

    /**
     * @var UserTransaction[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\UserTransaction", mappedBy="user")
     */
    private $userTransactions;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->betSlips = new ArrayCollection();
        $this->userTransactions = new ArrayCollection();
    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return User
     */
    public function setStatus(int $status): User
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Set nationalIdentificationNumber
     *
     * @param string $nationalIdentificationNumber
     *
     * @return User
     */
    public function setNationalIdentificationNumber($nationalIdentificationNumber)
    {
        $this->nationalIdentificationNumber = $nationalIdentificationNumber;

        return $this;
    }

    /**
     * Get nationalIdentificationNumber
     *
     * @return string
     */
    public function getNationalIdentificationNumber()
    {
        return $this->nationalIdentificationNumber;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        $this->password = null;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set country
     *
     * @param integer $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return User
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set performanceFactor
     *
     * @param integer $performanceFactor
     *
     * @return User
     */
    public function setPerformanceFactor($performanceFactor)
    {
        $this->performanceFactor = $performanceFactor;

        return $this;
    }

    /**
     * Get performanceFactor
     *
     * @return int
     */
    public function getPerformanceFactor()
    {
        return $this->performanceFactor;
    }

    /**
     * Set userBudget
     *
     * @param UserBudget $userBudget
     *
     * @return User
     */
    public function setUserBudget($userBudget)
    {
        $this->userBudget = $userBudget;

        return $this;
    }

    /**
     * Get userBudget
     *
     * @return UserBudget
     */
    public function getUserBudget()
    {
        return $this->userBudget;
    }

    /**
     * @return BetSlip[]|ArrayCollection
     */
    public function getBetSlips()
    {
        return $this->betSlips;
    }

    /**
     * @param BetSlip[]|ArrayCollection $betSlips
     *
     * @return User
     */
    public function setBetSlips($betSlips)
    {
        $this->betSlips = $betSlips;

        return $this;
    }

    /**
     * @return ArrayCollection|UserTransaction[]
     */
    public function getUserTransactions()
    {
        return $this->userTransactions;
    }

    /**
     * @param ArrayCollection|UserTransaction[] $userTransactions
     * @return User
     */
    public function setUserTransactions($userTransactions)
    {
        $this->userTransactions = $userTransactions;

        return $this;
    }
}

