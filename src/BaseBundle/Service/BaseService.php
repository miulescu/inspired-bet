<?php


namespace BaseBundle\Service;

use BaseBundle\Entity\ApplicationSetting;
use BaseBundle\Entity\User;
use BaseBundle\Repository\ApplicationSettingRepository;
use BaseBundle\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\UserBudget;
use UserBundle\Entity\UserTransaction;

/**
 * Class BaseService
 * @package BaseBundle\Service
 */
class BaseService
{
    /** @const ID */
    const ID = 'base.base_service';

    /** @var  AuthorizationChecker */
    private $authorizationChecker;

    /** @var  AuthenticationUtils */
    private $authenticationUtils;

    /** @var  TranslatorInterface */
    private $translator;

    /** @var  GuardAuthenticatorHandler */
    private $guardAuthenticatorHandler;

    /** @var  LoginFormAuthenticator */
    private $loginFormAuthenticator;

    /** @var  EntityManager */
    private $entityManager;

    /** @var  ApiService */
    private $apiService;

    /** @var  TokenStorage */
    private $tokenStorage;

    /**
     * @return bool
     */
    public function isUserLogged()
    {
        return $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY');
    }

    /**
     * @return array
     */
    public function getLoginDetails()
    {
        $error = $this->authenticationUtils->getLastAuthenticationError();
        $lastUsername = $this->authenticationUtils->getLastUsername();

        if ($error) {
            $error = $this->translator->trans($error->getMessageKey(), $error->getMessageData(), 'security');
        }

        return [$error, $lastUsername];
    }

    /**
     * @param $sum
     *
     * @return string
     */
    public function addCredit($sum)
    {
        if (!is_numeric($sum) || $sum < 0) {
            throw new Exception('Please reenter the sum you want to add');
        }

        /** @var User $user */
        $user = $this->getUser();
        $userBudget = $user->getUserBudget();

        $userTransaction = new UserTransaction();
        $userTransaction
            ->setUser($user)
            ->setType(UserTransaction::TYPE_DEPOSIT)
            ->setDetails('Deposit on add credit')
            ->setStatus(UserTransaction::STATUS_ACTIVE)
            ->setAmount($sum);

        $userBudget->setBudget($userBudget->getBudget() + $sum);

        $user->getUserTransactions()->add($userTransaction);
        $userBudget->getUserTransactions()->add($userTransaction);

        $this->entityManager->persist($userTransaction);
        $this->entityManager->flush();

        return 'All good! Budget was found with '.$sum;
    }

    /**
     * @param $sum
     *
     * @return string
     */
    public function creditWithdrawal($sum)
    {
        /** @var User $user */
        $user = $this->getUser();
        $userBudget = $user->getUserBudget();

        if (!is_numeric($sum) || $sum < 0 || $userBudget->getBudget() < $sum) {
            throw new Exception('Please reenter the sum you want to withdraw');
        }

        $userTransaction = new UserTransaction();
        $userTransaction
            ->setUser($user)
            ->setType(UserTransaction::TYPE_WITHDRAWAL)
            ->setDetails('Withdraw credit')
            ->setStatus(UserTransaction::STATUS_ACTIVE)
            ->setAmount($sum);

        $userBudget->setBudget($userBudget->getBudget() - $sum);

        $user->getUserTransactions()->add($userTransaction);
        $userBudget->getUserTransactions()->add($userTransaction);

        $this->entityManager->persist($userTransaction);
        $this->entityManager->flush();

        return 'All good!';
    }

    /**
     * @param User $user
     * @param Request $request
     *
     * @return null|Response
     */
    public function saveAndAuthenticateNewUser(User $user,Request $request)
    {
        $userTransaction = new UserTransaction();
        $userTransaction
            ->setUser($user)
            ->setType(UserTransaction::TYPE_DEPOSIT)
            ->setDetails('Deposit on create accont')
            ->setStatus(UserTransaction::STATUS_ACTIVE)
            ->setAmount($request->request->get('register')['budget']);

        $userBudget = new UserBudget();
        $userBudget
            ->setUser($user)
            ->setStatus(UserBudget::STATUS_ACTIVE)
            ->setBudget($request->request->get('register')['budget']);

        $userBudget->getUserTransactions()->add($userTransaction);
        $userTransaction->setUserBudget($userBudget);

        $user->setStatus(User::STATUS_ACTIVE);
        $user->setRoles(['ROLE_USER']);
        $user->setUserBudget($userBudget);

        $this->entityManager->persist($userTransaction);
        $this->entityManager->persist($userBudget);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->guardAuthenticatorHandler
            ->authenticateUserAndHandleSuccess($user, $request, $this->loginFormAuthenticator, 'main');
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public function getApplicationSetting(string $key)
    {
        /** @var ApplicationSettingRepository $applicationSettingRepository */
        $applicationSettingRepository = $this->entityManager->getRepository(ApplicationSetting::class);

        /** @var ApplicationSetting $applicationSetting */
        $applicationSetting = $applicationSettingRepository->findOneBy([
            'name' => $key,
            'status' => ApplicationSetting::STATUS_ACTIVE,
        ]);

        if (!$applicationSetting instanceof ApplicationSetting) {
            throw new Exception('The key given for get setting was not found or does not exist');
        }

        return $applicationSetting->getValue();
    }

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     */
    public function setAuthenticationUtils(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param GuardAuthenticatorHandler $guardAuthenticatorHandler
     */
    public function setGuardAuthenticatorHandler(GuardAuthenticatorHandler $guardAuthenticatorHandler)
    {
        $this->guardAuthenticatorHandler = $guardAuthenticatorHandler;
    }

    /**
     * @param LoginFormAuthenticator $loginFormAuthenticator
     */
    public function setLoginFormAuthenticator(LoginFormAuthenticator $loginFormAuthenticator)
    {
        $this->loginFormAuthenticator = $loginFormAuthenticator;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ApiService $apiService
     */
    public function setApiService(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param TokenStorage $tokenStorage
     */
    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
}