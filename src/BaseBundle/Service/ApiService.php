<?php


namespace BaseBundle\Service;


use GuzzleHttp\Client;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiService
 * @package BaseBundle\Service
 */
class ApiService
{
    /** @const ID */
    const ID = 'base.api_service';

    /** @const string */
    const API_BASE_URI = 'https://apifootball.com/api';

    /** @var  string */
    private $apiKey;

    /**
     * @param string $action
     * @param array $params
     *
     * @return string
     */
    public function getFootballData(string $action, array $params = array())
    {
        $client = new Client(['timeout' => 300, 'connect_timeout' => 15]);

        $params['APIkey'] = $this->apiKey;
        $params['action'] = $action;

        $response = $client->request(
            Request::METHOD_GET,
            ApiService::API_BASE_URI,
            ['query' => $params]
        );

        return json_encode([
            'statusCode' => $response->getStatusCode(),
            'response' => json_decode($response->getBody(), true),
        ]);
    }

    /**
     * @param $response
     */
    public function validateResponse($response)
    {
        if (!is_array($response) || $response['statusCode'] !== Response::HTTP_OK) {
            throw new Exception("Api could not be accessed, please try again later");
        }

        if (array_key_exists('error', $response)) {
            throw new Exception($response['message']);
        }
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }
}