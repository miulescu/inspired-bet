<?php

namespace UserBundle\Command;


use BetBundle\Service\BetService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateBetSlipsCommand
 * @package UserBundle\Command
 */
class UpdateBetSlipsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:update:betslips')
            ->setDescription('Update users bet slips statuses');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var BetService $betService */
        $betService = $this->getContainer()->get(BetService::ID);

        $output->writeln('====================================================');
        $message = 'All good! users bet slips statuses was updated!';

        try {
            $betService->updateBetSlipsStatus();
        } catch (\Throwable $t) {
            $message = $t->getMessage();
        }

        $output->writeln($message);
    }
}