<?php

namespace UserBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use BaseBundle\Entity\User;
use BetBundle\Entity\BetSlip;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserTransaction
 *
 * @ORM\Table(name="user_transaction")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserTransactionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class UserTransaction
{
    use TimestampableTrait;

    const TYPE_DEPOSIT = 1;

    const TYPE_WITHDRAWAL = 2;

    const TYPE_STAKE = 3;

    const TYPE_INCOME = 4;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User", inversedBy="userTransactions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string", length=255, nullable=true)
     */
    private $details;

    /**
     * @var UserBudget
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\UserBudget", inversedBy="userTransactions")
     * @ORM\JoinColumn(name="user_budget_id", referencedColumnName="id")
     */
    private $userBudget;

    /**
     * @var BetSlip
     *
     * @ORM\OneToOne(targetEntity="BetBundle\Entity\BetSlip", mappedBy="userTransaction")
     */
    private $betSlip;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserTransaction
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return UserTransaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return UserTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return UserTransaction
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return UserBudget
     */
    public function getUserBudget()
    {
        return $this->userBudget;
    }

    /**
     * @param UserBudget $userBudget
     * @return UserTransaction
     */
    public function setUserBudget($userBudget)
    {
        $this->userBudget = $userBudget;

        return $this;
    }

    /**
     * @return BetSlip
     */
    public function getBetSlip(): BetSlip
    {
        return $this->betSlip;
    }

    /**
     * @param BetSlip $betSlip
     *
     * @return UserTransaction
     */
    public function setBetSlip($betSlip)
    {
        $this->betSlip = $betSlip;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return UserTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}

