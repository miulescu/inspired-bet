<?php

namespace UserBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use BaseBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserBudget
 *
 * @ORM\Table(name="user_budget")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserBudgetRepository")
 * @ORM\HasLifecycleCallbacks
 */
class UserBudget
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="BaseBundle\Entity\User", inversedBy="userBudget")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="budget", type="float")
     */
    private $budget;

    /**
     * @var UserTransaction|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\UserTransaction", mappedBy="userBudget")
     */
    private $userTransactions;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * UserBudget constructor.
     */
    public function __construct()
    {
        $this->userTransactions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserBudget
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set budget
     *
     * @param float $budget
     *
     * @return UserBudget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return float
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @return ArrayCollection
     */
    public function getUserTransactions()
    {
        return $this->userTransactions;
    }

    /**
     * @param ArrayCollection|UserTransaction $userTransactions
     * @return UserBudget
     */
    public function setUserTransactions($userTransactions)
    {
        $this->userTransactions = $userTransactions;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return UserBudget
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}

