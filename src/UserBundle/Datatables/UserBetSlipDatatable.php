<?php


namespace UserBundle\Datatables;


use BetBundle\Entity\BetSlip;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Style;

class UserBetSlipDatatable extends AbstractDatatable
{
    public function getLineFormatter()
    {
        $formatter = function ($line) {

            $line['status'] = $this->statusFormatter($line['status']);

            return $line;
        };

        return $formatter;
    }

    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'language_by_locale' => true
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'page_length' => 15,
            'paging_type' => Style::NUMBERS_PAGINATION,
            'renderer' => 'bootstrap',
            'scroll_collapse' => true,
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => false,
        ));

        $this->features->set(array(
            'auto_width' => true,
            'info' => false,
            'length_change' => false,
            'ordering' => false,
            'paging' => true,
            'searching' => false,
            'processing' => true
        ));

        $this->extensions->set(array(
            'responsive' => true,
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
                'title' => 'ID'
            ))
            ->add('income', Column::class, array(
                'title' => $this->translator->trans('user.page.income')
            ))
            ->add('status', Column::class, array(
                'title' => 'Status'
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('user.page.actionsTitle'),
                'start_html' => '<div class="actions">',
                'end_html' => '</div>',
                'actions' => array(
                    array(
                        'route' => 'user_bets_betslip_show',
                        'route_parameters' => array(
                            'betSlip' => 'id'
                        ),
                        'label' => $this->translator->trans('user.page.actions'),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button',
                        ),
                    )
                )
            ))
        ;
    }

    public function statusFormatter($statusId)
    {
        switch ($statusId) {
            case BetSlip::STATUS_NEW :
                return '<span class="label label-default">'.$this->translator->trans('user.page.statusNew').'</span>';
            case BetSlip::STATUS_LOST :
                return '<span class="label label-danger">'.$this->translator->trans('user.page.statusLost').'</span>';
            case BetSlip::STATUS_WON :
                return '<span class="label label-success">'.$this->translator->trans('user.page.statusWon').'</span>';
            case BetSlip::STATUS_IN_PROGRESS :
                return '<span class="label label-warning">'.$this->translator->trans('user.page.statusIP').'</span>';
        }
    }

    public function getEntity()
    {
        return BetSlip::class;
    }

    public function getName()
    {
        return 'userBetSlip_datatable';
    }
}