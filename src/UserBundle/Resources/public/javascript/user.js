function addCredit() {
    var url = Routing.generate('user_addCreditAjax', {'_locale': locale});

    $.ajax({
        type: "POST",
        url: url,
        data: {"sum": $('#sumAdd').val()},
        dataType: "json",
        success: function (response) {
            if (response.error === false) {
                $('#addCreditModal').modal('hide');
                addNotification(response.message, 'info');
            } else {
                $('#addCreditModal').modal('hide');
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            $('#addCreditModal').modal('hide');
            addNotification(errorThrown, 'danger');
        }
    })
}

function creditWithdrawal() {
    var url = Routing.generate('user_creditWithdrawalAjax', {'_locale': locale});

    $.ajax({
        type: "POST",
        url: url,
        data: {"sum": $('#sumWithdraw').val()},
        dataType: "json",
        success: function (response) {
            if (response.error === false) {
                $('#creditWithdrawalModal').modal('hide');
                addNotification(response.message, 'info');
            } else {
                $('#creditWithdrawalModal').modal('hide');
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            $('#creditWithdrawalModal').modal('hide');
            addNotification(errorThrown, 'danger');
        }
    })
}
