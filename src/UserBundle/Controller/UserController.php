<?php

namespace UserBundle\Controller;

use BaseBundle\Service\BaseService;
use BetBundle\Entity\BetSlip;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Datatables\UserBetSlipDatatable;

/**
 * Class UserController
 * @package UserBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user_showDetails")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showDetailsAction(Request $request)
    {
        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(UserBetSlipDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $datatableQueryBuilder->buildQuery();

            $qb = $datatableQueryBuilder->getQb();
            $qb
                ->innerJoin('betslip.user', 'u')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $this->getUser()->getId());

            return $responseService->getResponse();
        }

        return $this->render(
            '@User/User/index.html.twig',
            [
                'datatable' => $datatable,
            ]
        );
    }

    /**
     * @Route("/bets/{betSlip}", name="user_bets_betslip_show")
     * @ParamConverter("betSlip", class="BetBundle:BetSlip")
     *
     * @param BetSlip $betSlip
     *
     * @return Response
     */
    public function showBetsFromBetSlipAction(BetSlip $betSlip)
    {
        return $this->render(
            '@User/User/games.html.twig',
            ['betSlip' => $betSlip]
        );
    }

    /**
     * @Route("/add-credit-ajax",
     *      name="user_addCreditAjax",
     *      options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addCreditAjaxAction(Request $request)
    {
        /** @var BaseService $baseService */
        $baseService = $this->get(BaseService::ID);

        try {
            $message = $baseService->addCredit($request->get('sum'));

            return new JsonResponse(['error' => false, 'message' => $message]);
        } catch (\Throwable $t) {
            return new JsonResponse(['error' => true, 'message' => $t->getMessage()]);
        }
    }

    /**
     * @Route("/credit-withdrawal-ajax",
     *      name="user_creditWithdrawalAjax",
     *      options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function creditWithdrawalAjaxAction(Request $request)
    {
        /** @var BaseService $baseService */
        $baseService = $this->get(BaseService::ID);

        try {
            $message = $baseService->creditWithdrawal($request->get('sum'));

            return new JsonResponse(['error' => false, 'message' => $message]);
        } catch (\Throwable $t) {
            return new JsonResponse(['error' => true, 'message' => $t->getMessage()]);
        }
    }
}
