<?php

namespace GameBundle\Controller;

use GameBundle\Datatables\GameDatatable;
use GameBundle\Entity\League;
use GameBundle\Entity\Sport;
use GameBundle\Service\GameService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GameController
 * @package GameBundle\Controller
 */
class GameController extends Controller
{
    /**
     * @Route("/sport/{code}", name="game_game_sportGamesListing")
     * @Method("GET")
     *
     * @ParamConverter("sport", class="GameBundle:Sport", options={"code" : "code"})
     *
     * @param Sport $sport
     * @param Request $request
     *
     * @return Response
     */
    public function sportGamesListingAction(Sport $sport, Request $request)
    {
        /** @var GameService $gameService */
        $gameService = $this->get(GameService::ID);

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(GameDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $datatableQueryBuilder->buildQuery();

            $qb = $datatableQueryBuilder->getQb();
            $qb
                ->innerJoin('league.competition', 'c')
                ->innerJoin('c.sport', 's')
                ->innerJoin('game.bets', 'b')
                ->andWhere('b.id IS NOT NULL')
                ->andWhere('s.code = :sport')
                ->setParameter('sport', $sport->getCode())
                ->andWhere('league in (:leagues)')
                ->setParameter('leagues', $gameService->getHighlightedLeagues());

            return $responseService->getResponse();
        }

        return $this->render('@Game/Game/sportGamesListing.html.twig',
            [
                'selectedSport' => $sport->getCode(),
                'competitions' => $gameService->getHighlightedCompetitions($sport->getCode()),
                'leagues' => $gameService->getHighlightedLeagues(),
                'datatable' => $datatable,
            ]
        );
    }

    /**
     * @Route("/league/{id}", name="game_game_leagueGamesListing")
     * @Method("GET")
     *
     * @ParamConverter("league", class="GameBundle:League")
     *
     * @param League $league
     * @param Request $request
     *
     * @return Response
     */
    public function leagueGamesListingAction(League $league, Request $request)
    {
        /** @var GameService $gameService */
        $gameService = $this->get(GameService::ID);

        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(GameDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $datatableQueryBuilder->buildQuery();

            $qb = $datatableQueryBuilder->getQb();
            $qb
                ->innerJoin('game.bets', 'b')
                ->andWhere('b.id IS NOT NULL')
                ->andWhere('league = :league')
                ->setParameter('league', $league);

            return $responseService->getResponse();
        }

        return $this->render('@Game/Game/leagueGamesListing.html.twig',
            [
                'selectedSport' => $league->getCompetition()->getSport()->getCode(),
                'competition' => $league->getCompetition(),
                'leagues' => $gameService->getHighlightedLeagues(),
                'selectedLeague' => $league->getId(),
                'datatable' => $datatable,
            ]
        );
    }

    /**
     * @Route("/sports-listing-ajax",
     *      name="game_game_sportsListingAjax",
     *      options={"expose"=true})
     *
     * @return JsonResponse
     */
    public function sportsListingAjaxAction()
    {
        /** @var GameService $gameService */
        $gameService = $this->get(GameService::ID);

        try {
            $view = $this->renderView('@Game/Game/navTabsSportsListing.html.twig',
                ['sportsCodes' => $gameService->getSportsCodes()]
            );

            return new JsonResponse(['error' => false, 'content' => $view]);
        } catch (\Throwable $t) {
            return new JsonResponse(['error' => true, 'message' => $t->getMessage()]);
        }
    }
}
