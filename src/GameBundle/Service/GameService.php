<?php


namespace GameBundle\Service;

use BaseBundle\Service\ApiService;
use BaseBundle\Service\BaseService;
use Doctrine\ORM\EntityManager;
use GameBundle\Entity\Competition;
use GameBundle\Entity\Game;
use GameBundle\Entity\GameDetails;
use GameBundle\Entity\League;
use GameBundle\Entity\Sport;
use GameBundle\Repository\CompetitionRepository;
use GameBundle\Repository\GameRepository;
use GameBundle\Repository\LeagueRepository;
use GameBundle\Repository\SportRepository;

/**
 * Class GameService
 * @package GameBundle\Service
 */
class GameService
{
    /** @const string */
    const ID = 'game.game_service';

    /** @var  EntityManager */
    private $entityManager;

    /** @var  ApiService */
    private $apiService;

    /** @var  BaseService */
    private $baseService;

    /**
     * Update competition table in database
     */
    public function updateCompetitions()
    {
        $response = json_decode($this->apiService->getFootballData('get_countries'), true);

        $this->apiService->validateResponse($response);

        /** @var CompetitionRepository $competitionRepository */
        $competitionRepository = $this->entityManager->getRepository(Competition::class);

        foreach ($response['response'] as $competitionDetails) {
            /** @var Competition $competition */
            $competition = $competitionRepository->find($competitionDetails['country_id']);

            if ($competition instanceof Competition) {
                continue;
            }

            $competition = new Competition();
            $competition
                ->setId($competitionDetails['country_id'])
                ->setName($competitionDetails['country_name'])
                ->setSport($this->entityManager->getReference(Sport::class, Sport::FOOTBALL_ID))
                ->setStatus(Competition::STATUS_ACTIVE)
                ->setCode(strtolower(substr($competitionDetails['country_name'],0,2)));

            $this->entityManager->persist($competition);
        }
        $this->entityManager->flush();
    }

    /**
     * @param null $competitionId
     */
    public function updateLeagues($competitionId = null)
    {
        $params = array();
        if (!empty($competitionId)) {
            $params = [
                'country_id' => $competitionId,
            ];
        }

        $response = json_decode(
            $this->apiService->getFootballData('get_leagues', $params),
            true
        );

        $this->apiService->validateResponse($response);

        /** @var LeagueRepository $leagueRepository */
        $leagueRepository = $this->entityManager->getRepository(League::class);

        foreach ($response['response'] as $leagueDetails) {
            /** @var League $league */
            $league = $leagueRepository->find($leagueDetails['league_id']);

            if ($league instanceof League) {
                continue;
            }

            $league = new League();
            $league
                ->setId($leagueDetails['league_id'])
                ->setName($leagueDetails['league_name'])
                ->setStatus(League::STATUS_ACTIVE)
                ->setCompetition($this->entityManager->getReference(Competition::class, $leagueDetails['country_id']));

            $this->entityManager->persist($league);
        }
        $this->entityManager->flush();
    }

    /**
     * @param $from
     * @param $to
     */
    public function updateGames($from, $to)
    {
        $response = json_decode(
            $this->apiService->getFootballData('get_events', ['from' => $from, 'to' => $to]),
            true
        );

        $this->apiService->validateResponse($response);

        /** @var GameRepository $gameRepository */
        $gameRepository = $this->entityManager->getRepository(Game::class);

        $cnt = 0;
        foreach ($response['response'] as $gameResponse) {
            /** @var Game $game */
            $game = $gameRepository->find($gameResponse['match_id']);

            if ($game instanceof Game) {
                if ($game->getStatus() == Game::STATUS_NOT_PLAYED_YET && $gameResponse['match_status'] == 'FT') {

                    $game->setStatus(Game::STATUS_FINISHED);

                    /** @var GameDetails $gameDetails */
                    $gameDetails = $game->getGameDetails();
                    $gameDetails->fromArray($gameResponse);
                }
                continue;
            }

            /** @var League $league */
            $league = $this->entityManager->find(League::class, $gameResponse['league_id']);

            if (!$league instanceof League) {
                $competition = $this->entityManager->find(Competition::class, $gameResponse['country_id']);

                $league = new League();
                $league
                    ->setId($gameResponse['league_id'])
                    ->setName($gameResponse['league_name'])
                    ->setStatus(League::STATUS_ACTIVE);

                if (!$competition instanceof Competition) {
                    $competition = new Competition();
                    $competition
                        ->setId($gameResponse['country_id'])
                        ->setName($gameResponse['country_name'])
                        ->setSport($this->entityManager->getReference(Sport::class, Sport::FOOTBALL_ID))
                        ->setStatus(Competition::STATUS_ACTIVE)
                        ->setCode(strtolower(substr($gameResponse['country_name'],0,2)));

                    $this->entityManager->persist($competition);
                }
                $league->setCompetition($competition);

                $this->entityManager->persist($league);
            }

            $game = new Game($gameResponse, $league);
            $gameDetails = $game->getGameDetails();

            $this->entityManager->persist($game);
            $this->entityManager->persist($gameDetails);

            $cnt++;
            if ($cnt % 20 == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @param $sport
     *
     * @return array|mixed
     */
    public function getHighlightedCompetitions($sport)
    {
        $highlightedCompetitions = json_decode(
            $this->baseService->getApplicationSetting('highlightedCompetitions')
        );

        /** @var CompetitionRepository $competitionRepository */
        $competitionRepository = $this->entityManager->getRepository(Competition::class);
        $highlightedCompetitions = $competitionRepository->getActiveCompetitionsBySportAndIds($sport ,$highlightedCompetitions);

        return $highlightedCompetitions;
    }

    /**
     * @return array|mixed
     */
    public function getHighlightedLeagues()
    {
        $highlightedLeagues = json_decode(
            $this->baseService->getApplicationSetting('highlightedLeagues')
        );

        /** @var LeagueRepository $leagueRepository */
        $leagueRepository = $this->entityManager->getRepository(League::class);
        $highlightedLeagues = $leagueRepository->getActiveLeaguesByIds($highlightedLeagues);

        return $highlightedLeagues;
    }

    public function getGridData($params)
    {
        return $this->entityManager->getRepository(Game::class)->getListDataTables($params);
    }

    /**
     * @return array
     */
    public function getSportsCodes()
    {
        /** @var SportRepository $sportRepository */
        $sportRepository = $this->entityManager->getRepository(Sport::class);

        return $sportRepository->getAllActiveSportsCodes();
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ApiService $apiService
     */
    public function setApiService(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param BaseService $baseService
     */
    public function setBaseService(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }
}