<?php

namespace GameBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Sport
 *
 * @ORM\Table(name="sport")
 * @ORM\Entity(repositoryClass="GameBundle\Repository\SportRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Sport
{
    use TimestampableTrait;

    /** @const int */
    const FOOTBALL_ID = 1;

    /** @const int */
    const TENNIS_ID = 2;

    /** @const int */
    const HANDBALL_ID = 3;

    /** @const int */
    const BASKETBALL_ID = 4;

    /** @const int */
    const HOCKEY_ID = 5;

    /** @const int */
    const VOLLEYBALL_ID = 6;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=20, unique=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="team_members", type="integer")
     */
    private $teamMembers;

    /**
     * @var Competition[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="GameBundle\Entity\Competition", mappedBy="sport")
     */
    private $competitions;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Sport
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getTeamMembers(): int
    {
        return $this->teamMembers;
    }

    /**
     * @param int $teamMembers
     * @return Sport
     */
    public function setTeamMembers(int $teamMembers): Sport
    {
        $this->teamMembers = $teamMembers;

        return $this;
    }

    /**
     * @return ArrayCollection|Competition[]
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Sport
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}

