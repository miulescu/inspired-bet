<?php

namespace GameBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Competition
 *
 * @ORM\Table(name="competition")
 * @ORM\Entity(repositoryClass="GameBundle\Repository\CompetitionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Competition
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=2)
     */
    private $code;

    /**
     * @var Sport
     *
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Sport", inversedBy="competitions")
     * @ORM\JoinColumn(name="sport_id", referencedColumnName="id")
     */
    private $sport;

    /**
     * @var League[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="GameBundle\Entity\League", mappedBy="competition")
     */
    private $leagues;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @param int $id
     *
     * @return Competition
     */
    public function setId(int $id): Competition
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Competition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Competition
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Sport
     */
    public function getSport(): Sport
    {
        return $this->sport;
    }

    /**
     * @param $sport
     * @return Competition
     */
    public function setSport($sport): Competition
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * @return ArrayCollection|League[]
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Competition
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}

