<?php

namespace GameBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use BetBundle\Entity\Bet;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="GameBundle\Repository\GameRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Game
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_NOT_PLAYED_YET = 0;

    /** @const int */
    const STATUS_FINISHED = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var League
     *
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\League", inversedBy="games")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    private $league;

    /**
     * @var string
     *
     * @ORM\Column(name="home_team_name", type="string", length=80)
     */
    private $homeTeamName;

    /**
     * @var string
     *
     * @ORM\Column(name="away_team_name", type="string", length=80)
     */
    private $awayTeamName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="game_date", type="date", nullable=false)
     */
    private $gameDate;

    /**
     * @var string
     *
     * @ORM\Column(name="game_time", type="string", length=10, nullable=false)
     */
    private $gameTime;

    /**
     * @var GameDetails
     *
     * @ORM\OneToOne(targetEntity="GameBundle\Entity\GameDetails", mappedBy="game")
     */
    private $gameDetails;

    /**
     * @var Bet[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BetBundle\Entity\Bet", mappedBy="game", cascade={"persist"})
     */
    private $bets;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * Game constructor.
     *
     * @param array $game
     * @param $league
     */
    public function __construct(array $game, $league)
    {
        $this->generateFromArray($game);
        $this->setLeague($league);

        $this->bets = new ArrayCollection();
    }

    /**
     * @param $array
     */
    private function generateFromArray($array)
    {
        $this
            ->setId($array['match_id'])
            ->setHomeTeamName($array['match_hometeam_name'])
            ->setAwayTeamName($array['match_awayteam_name'])
            ->setGameDate(new \DateTime($array['match_date']))
            ->setGameTime($array['match_time'])
            ->setStatus(empty($array['match_status']) ? self::STATUS_NOT_PLAYED_YET : self::STATUS_FINISHED);

        $gameDetails = new GameDetails($array, $this);
        $this->setGameDetails($gameDetails);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return Game
     */
    public function setId($id): Game
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return League
     */
    public function getLeague(): League
    {
        return $this->league;
    }

    /**
     * @param $league
     * @return Game
     */
    public function setLeague($league): Game
    {
        $this->league = $league;
        return $this;
    }

    /**
     * @return string
     */
    public function getHomeTeamName(): string
    {
        return $this->homeTeamName;
    }

    /**
     * @param string $homeTeamName
     * @return Game
     */
    public function setHomeTeamName(string $homeTeamName): Game
    {
        $this->homeTeamName = $homeTeamName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAwayTeamName(): string
    {
        return $this->awayTeamName;
    }

    /**
     * @param string $awayTeamName
     * @return Game
     */
    public function setAwayTeamName(string $awayTeamName): Game
    {
        $this->awayTeamName = $awayTeamName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getGameDate(): \DateTime
    {
        return $this->gameDate;
    }

    /**
     * @param gameDate
     * @return Game
     */
    public function setGameDate($gameDate): Game
    {
        $this->gameDate = $gameDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getGameTime()
    {
        return $this->gameTime;
    }

    /**
     * @param $gameTime
     * @return Game
     */
    public function setGameTime($gameTime): Game
    {
        $this->gameTime = $gameTime;
        return $this;
    }

    /**
     * @return GameDetails
     */
    public function getGameDetails(): GameDetails
    {
        return $this->gameDetails;
    }

    /**
     * @param $gameDetails
     * @return Game
     */
    public function setGameDetails($gameDetails): Game
    {
        $this->gameDetails = $gameDetails;
        return $this;
    }

    /**
     * @return Bet[]|ArrayCollection
     */
    public function getBets()
    {
        return $this->bets;
    }

    /**
     * @param Bet[]|ArrayCollection $bets
     * @return Game
     */
    public function setBets($bets)
    {
        $this->bets = $bets;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Game
     */
    public function setStatus(int $status): Game
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return self::getHomeTeamName()." - ".self::getAwayTeamName();
    }
}

