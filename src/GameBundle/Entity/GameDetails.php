<?php

namespace GameBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * GameDetails
 *
 * @ORM\Table(name="game_details")
 * @ORM\Entity(repositoryClass="GameBundle\Repository\GameDetailsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class GameDetails
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_NOT_PLAYED_YET = 0;

    /** @const int */
    const STATUS_FINISHED = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Game
     *
     * @ORM\OneToOne(targetEntity="GameBundle\Entity\Game", inversedBy="gameDetails")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    private $game;

    /**
     * @var int
     *
     * @ORM\Column(name="home_team_score", type="integer")
     */
    private $homeTeamScore;

    /**
     * @var int
     *
     * @ORM\Column(name="away_team_score", type="integer")
     */
    private $awayTeamScore;

    /**
     * @var int
     *
     * @ORM\Column(name="total_goals", type="integer")
     */
    private $totalGoals;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * GameDetails constructor.
     *
     * @param array $gameDetails
     * @param $game
     */
    public function __construct(array $gameDetails, $game)
    {
        $this->fromArray($gameDetails);
        $this->setGame($game);
    }

    /**
     * @param $array
     */
    public function fromArray($array)
    {
        $this
            ->setHomeTeamScore((int) $array['match_hometeam_score'])
            ->setAwayTeamScore((int) $array['match_awayteam_score'])
            ->setTotalGoals((int) $array['match_hometeam_score'] + (int) $array['match_awayteam_score'])
            ->setStatus(empty($array['match_status']) ? self::STATUS_NOT_PLAYED_YET : self::STATUS_FINISHED);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GameDetails
     */
    public function setId(int $id): GameDetails
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Game
     */
    public function getGame(): Game
    {
        return $this->game;
    }

    /**
     * @param $game
     * @return GameDetails
     */
    public function setGame($game): GameDetails
    {
        $this->game = $game;
        return $this;
    }

    /**
     * @return int
     */
    public function getHomeTeamScore(): int
    {
        return $this->homeTeamScore;
    }

    /**
     * @param int $homeTeamScore
     * @return GameDetails
     */
    public function setHomeTeamScore(int $homeTeamScore): GameDetails
    {
        $this->homeTeamScore = $homeTeamScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getAwayTeamScore(): int
    {
        return $this->awayTeamScore;
    }

    /**
     * @param int $awayTeamScore
     * @return GameDetails
     */
    public function setAwayTeamScore(int $awayTeamScore): GameDetails
    {
        $this->awayTeamScore = $awayTeamScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalGoals(): int
    {
        return $this->totalGoals;
    }

    /**
     * @param int $totalGoals
     * @return GameDetails
     */
    public function setTotalGoals(int $totalGoals): GameDetails
    {
        $this->totalGoals = $totalGoals;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return GameDetails
     */
    public function setStatus(int $status): GameDetails
    {
        $this->status = $status;
        return $this;
    }
}

