<?php

namespace GameBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * League
 *
 * @ORM\Table(name="league")
 * @ORM\Entity(repositoryClass="GameBundle\Repository\LeagueRepository")
 * @ORM\HasLifecycleCallbacks
 */
class League
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var Competition
     *
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Competition", inversedBy="leagues")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id")
     */
    private $competition;

    /**
     * @var Game[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="GameBundle\Entity\Game", mappedBy="league")
     */
    private $games;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @param int $id
     *
     * @return League
     */
    public function setId(int $id): League
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return League
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Competition
     */
    public function getCompetition(): Competition
    {
        return $this->competition;
    }

    /**
     * @param $competition
     * @return League
     */
    public function setCompetition($competition): League
    {
        $this->competition = $competition;
        return $this;
    }

    /**
     * @return ArrayCollection|Game[]
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return League
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}

