// Render the sports navTabs in a div located in body block with id: sportsListing
function navTabsSportsListing() {
    var url = Routing.generate('game_game_sportsListingAjax', {'_locale': locale });

    $.ajax({
        type: "GET",
        url: url,
        success: function (response) {
            if (response.error === false) {
                $('#sportsListing').html(response.content);
            } else {
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            addNotification(errorThrown, 'danger');
        }
    })
}