<?php


namespace GameBundle\Command;


use GameBundle\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetCompetitionsCommand
 * @package GameBundle\Command
 */
class GetCompetitionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('api:get:competitions')
            ->setDescription('Update competition table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var GameService $gameService */
        $gameService = $this->getContainer()->get(GameService::ID);

        $output->writeln('====================================================');
        $message = 'All good! competition table was updated!';

        try {
            $gameService->updateCompetitions();
        } catch (\Throwable $t) {
            $message = $t->getMessage();
        }

        $output->writeln($message);
    }
}