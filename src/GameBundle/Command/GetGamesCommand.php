<?php


namespace GameBundle\Command;


use GameBundle\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetGamesCommand
 * @package GameBundle\Command
 */
class GetGamesCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('api:get:games')
            ->setDescription('Update game and game_details tables')
            ->addArgument('from',
                InputArgument::REQUIRED,
                'From date yyyy-mm-dd')
            ->addArgument('to',
                InputArgument::REQUIRED,
                'To date yyyy-mm-dd');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var GameService $gameService */
        $gameService = $this->getContainer()->get(GameService::ID);

        $output->writeln('====================================================');
        $message = 'All good! game and game_details tables was updated!';

        try {
            $gameService->updateGames(
                $input->getArgument('from'),
                $input->getArgument('to'));
        } catch (\Throwable $t) {
            $message = $t->getMessage();
        }

        $output->writeln($message);
    }
}