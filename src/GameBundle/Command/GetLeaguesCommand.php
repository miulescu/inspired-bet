<?php


namespace GameBundle\Command;


use GameBundle\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetLeaguesCommand
 * @package GameBundle\Command
 */
class GetLeaguesCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('api:get:leagues')
            ->setDescription('Update league table')
            ->addArgument('competition_id',
                InputArgument::OPTIONAL,
                'Update leagues only for competition id');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var GameService $gameService */
        $gameService = $this->getContainer()->get(GameService::ID);

        $output->writeln('====================================================');
        $message = 'All good! league table was updated!';

        try {
            $gameService->updateLeagues($input->getArgument('competition_id'));
        } catch (\Throwable $t) {
            $message = $t->getMessage();
        }

        $output->writeln($message);
    }
}