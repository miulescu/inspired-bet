<?php

namespace GameBundle\Datatables;

use BetBundle\Entity\Bet;
use BetBundle\Entity\BetType;
use BetBundle\Repository\BetRepository;
use GameBundle\Entity\Game;
use GameBundle\Entity\League;
use GameBundle\Repository\GameRepository;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;

/**
 * Class GameDatatable
 *
 * @package GameBundle\Datatables
 */
class GameDatatable extends AbstractDatatable
{
    /**
     * @return \Closure
     */
    public function getLineFormatter()
    {
        $router = $this->router;
        $em = $this->em;

        $formatter = function ($line) use ($router, $em) {
            /** @var GameRepository $gameRepository */
            $gameRepository = $em->getRepository(Game::class);
            /** @var Game $game */
            $game = $gameRepository->find($line['id']);

            $line['game_details'] = $this->gameFormatter($game);
            $line['league_competition'] = $this->leagueFormatter($game->getLeague());
            $line['odd_1'] = $this->oddFormatter($line['id'], BetType::TYPE_1);
            $line['odd_x'] = $this->oddFormatter($line['id'], BetType::TYPE_X);
            $line['odd_2'] = $this->oddFormatter($line['id'], BetType::TYPE_2);

            return $line;
        };

        return $formatter;
    }

    /**
     * @param array $options
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'language_by_locale' => true
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'page_length' => 15,
            'paging_type' => Style::NUMBERS_PAGINATION,
            'renderer' => 'bootstrap',
            'scroll_collapse' => true,
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => false,
        ));

        $this->features->set(array(
            'auto_width' => true,
            'info' => false,
            'length_change' => false,
            'ordering' => false,
            'paging' => true,
            'searching' => false,
            'processing' => true
        ));

        $this->extensions->set(array(
            'responsive' => true,
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
                'visible' => false,
            ))
            ->add('league.id', Column::class, array(
                'visible' => false,
            ))
            ->add('game_details', VirtualColumn::class, array(
                'title' => $this->translator->trans('games.game'),
            ))
            ->add('league_competition', VirtualColumn::class, array(
                'title' => $this->translator->trans('games.league'),
                'width' => '18%',
            ))
            ->add('odd_1', VirtualColumn::class, array(
                'title' => '1',
                'width' => '4%',
            ))
            ->add('odd_x', VirtualColumn::class, array(
                'title' => 'X',
                'width' => '4%',
            ))
            ->add('odd_2', VirtualColumn::class, array(
                'title' => '2',
                'width' => '4%',
            ));
    }

    /**
     * @param Game $game
     *
     * @return string
     */
    protected function gameFormatter(Game $game)
    {
        $gameFormatter =
            $game->getHomeTeamName().
            ' - '
            .$game->getAwayTeamName().
            '<br><span style="font-size:11px;">'
            .$game->getGameDate()->format('d-m').
            ' '
            .$game->getGameTime().'</span>';

        return $gameFormatter;
    }

    /**
     * @param League $league
     *
     * @return string
     */
    protected function leagueFormatter(League $league)
    {
        $leagueFormatter =
            $league->getName().
            '<br><span style="font-size:11px;">'
            .$league->getCompetition()->getName().
            '</span>';

        return $leagueFormatter;
    }

    /**
     * @param int $gameId
     * @param int $betTypeId
     *
     * @return mixed
     */
    public function oddFormatter(int $gameId, int $betTypeId)
    {
        /** @var BetRepository $betRepository */
        $betRepository = $this->em->getRepository(Bet::class);

        $bet = $betRepository->findOneByGameAndBetType($gameId, $betTypeId);

        $oddFormatter = '<button type="button" onclick="addNewBet('.$bet['id'].')" class="btn btn-default btn-sm">'.$bet['odd'].'</button>';

        return $oddFormatter;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return Game::class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'game_datatable';
    }
}
