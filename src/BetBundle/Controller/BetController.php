<?php

namespace BetBundle\Controller;

use BetBundle\Form\BetSlipType;
use BetBundle\Service\BetService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BetController extends Controller
{
    /**
     * @Route("/bet-slip-ajax",
     *      name="bet_betSlipAjax",
     *      options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function newBetSlipAjaxAction(Request $request)
    {
        /** @var BetService $betService */
        $betService = $this->get(BetService::ID);

        try {
            if ($request->getMethod() == Request::METHOD_POST
                && $request->request->get('betId')) {
                $betService->setBetInSession($request->request->get('betId'));
            }

            $betSlip = $betService->processBetsForBetSlip();
            $betSlipForm = $this->createForm(BetSlipType::class, $betSlip);

            $view = $this->renderView(
                '@Bet/Bet/betSlip.html.twig', [
                    'betSlipForm' => $betSlipForm->createView(),
                    ]);

            return new JsonResponse(['error' => false, 'content' => $view]);
        } catch (\Throwable $t) {
            return new JsonResponse(['error' => true, 'message' => $t->getMessage()]);
        }
    }

    /**
     * @Route("/remove-session-variable-ajax",
     *      name="bet_removeSessionVariableAjax",
     *      options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeSessionVariableAjaxAction(Request $request)
    {
        /** @var BetService $betService */
        $betService = $this->get(BetService::ID);

        try {
            $betService->removeSessionVariable($request->get('betId'));

            return new JsonResponse(['error' => false]);
        } catch (\Throwable $t) {
            return new JsonResponse(['error' => true, 'message' => $t->getMessage()]);
        }
    }

    /**
     * @Route("/save-bet-slip-ajax",
     *      name="bet_saveBetSlipAjax",
     *      options={"expose"=true})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveBetSlipAjaxAction(Request $request)
    {
        /** @var BetService $betService */
        $betService = $this->get(BetService::ID);

        try {
            $message = $betService->saveBetSlip($request->get('stake'));

            return new JsonResponse(['error' => false, 'message' => $message]);
        } catch (\Throwable $t) {
            return new JsonResponse(['error' => true, 'message' => $t->getMessage()]);
        }
    }
}
