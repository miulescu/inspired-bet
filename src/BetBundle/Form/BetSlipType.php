<?php

namespace BetBundle\Form;

use BetBundle\Entity\BetSlip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BetSlipType
 * @package BetBundle\Form
 */
class BetSlipType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stake',NumberType::class,
                [
                    'label' => 'bets.stake',
                    'attr' => [
                        'onkeyup' => 'calculatePotentialGain();',
                    ]
                ]
            )
            ->add('income',HiddenType::class)
            ->add('bets',
                CollectionType::class,
                [
                    'entry_type' => BetFormType::class,
                    'by_reference' => false,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BetSlip::class,
            'csrf_protection' => false,
        ]);
    }
}
