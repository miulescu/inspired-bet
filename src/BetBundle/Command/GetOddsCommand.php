<?php


namespace BetBundle\Command;


use BetBundle\Service\BetService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetOddsCommand
 * @package BetBundle\Command
 */
class GetOddsCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('api:get:odds')
            ->setDescription('Update the bet table')
            ->addArgument('from',
                InputArgument::REQUIRED,
                'From date yyyy-mm-dd')
            ->addArgument('to',
                InputArgument::REQUIRED,
                'To date yyyy-mm-dd');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var BetService $betService */
        $betService = $this->getContainer()->get(BetService::ID);

        $output->writeln('====================================================');
        $message = 'All good! bet table was updated!';

        try {
            $betService->updateOdds(
                $input->getArgument('from'),
                $input->getArgument('to'));
        } catch (\Throwable $t) {
            $message = $t->getMessage();
        }

        $output->writeln($message);
    }
}