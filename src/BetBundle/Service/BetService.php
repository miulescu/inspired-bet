<?php


namespace BetBundle\Service;

use BaseBundle\Entity\User;
use BaseBundle\Service\ApiService;
use BaseBundle\Service\BaseService;
use BetBundle\Entity\Bet;
use BetBundle\Entity\BetSlip;
use BetBundle\Entity\BetType;
use BetBundle\Repository\BetRepository;
use BetBundle\Repository\BetSlipRepository;
use BetBundle\Repository\BetTypeRepository;
use Doctrine\ORM\EntityManager;
use GameBundle\Entity\Game;
use GameBundle\Entity\GameDetails;
use GameBundle\Repository\GameRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\UserBudget;
use UserBundle\Entity\UserTransaction;
use UserBundle\Repository\UserTransactionRepository;


/**
 * Class BetService
 * @package BetBundle\Service
 */
class BetService
{
    /** @const string */
    const ID = 'bet.bet_service';

    /** @var  EntityManager */
    private $entityManager;

    /** @var  ApiService */
    private $apiService;

    /** @var  Session */
    private $session;

    /** @var  TranslatorInterface */
    private $translator;

    /** @var  BaseService */
    private $baseService;

    /**
     * @param $from
     * @param $to
     */
    public function updateOdds($from, $to)
    {
        $response = json_decode(
            $this->apiService->getFootballData('get_odds', ['from' => $from, 'to' => $to]),
            true
        );

        $this->apiService->validateResponse($response);

        /** @var GameRepository $gameRepository */
        $gameRepository = $this->entityManager->getRepository(Game::class);

        /** @var BetTypeRepository $betTypeRepository */
        $betTypeRepository = $this->entityManager->getRepository(BetType::class);
        /** @var BetType[] $betTypes */
        $betTypes = $betTypeRepository->findBy(['status' => BetType::STATUS_ACTIVE]);

        $cnt = 0;
        foreach ($response['response'] as $oddResponse) {
            if ($oddResponse['odd_bookmakers'] === 'bwin') {
                /** @var Game $game */
                $game = $gameRepository->find($oddResponse['match_id']);

                if ($game instanceof Game) {
                    if ($game->getBets()->count() < count($betTypes)) {
                        /** @var BetType $betType */
                        foreach ($betTypes as $betType) {
                            $bet = new Bet();
                            $bet
                                ->setGame($game)
                                ->setStatus(Bet::STATUS_ACTIVE)
                                ->setBetType($this->entityManager->getReference(BetType::class, $betType->getId()));
                            switch ($betType->getCode()) {
                                case '1':
                                    $bet->setOdd((float) $oddResponse['odd_1']);
                                    break;
                                case 'x':
                                    $bet->setOdd((float) $oddResponse['odd_x']);
                                    break;
                                case '2':
                                    $bet->setOdd((float) $oddResponse['odd_2']);
                                    break;
                                case 'yes':
                                    $bet->setOdd((float) $oddResponse['bts_yes']);
                                    break;
                                case 'no':
                                    $bet->setOdd((float) $oddResponse['bts_no']);
                                    break;
                                case 'u2.5':
                                    $bet->setOdd((float) $oddResponse['u+2.5']);
                                    break;
                                case 'o2.5':
                                    $bet->setOdd((float) $oddResponse['o+2.5']);
                                    break;
                            }
                            if (empty($bet->getOdd())) {
                                $bet->setOdd(1);
                            }
                            $this->entityManager->persist($bet);
                        }
                    }
                    $cnt++;
                    if ($cnt % 20 == 0) {
                        $this->entityManager->flush();
                        $this->entityManager->clear();
                    }
                }
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @param $betId
     *
     * @return bool
     */
    public function setBetInSession($betId)
    {
        if (!$this->session->has('betIds')) {
            $this->session->set('betIds', [$betId]);
            return true;
        }

        $betIds = $this->session->get('betIds');

        if (in_array($betId, $betIds)) {
            throw new Exception($this->translator->trans('bets.gameError'));
        }

        $betIds[] = $betId;

        $this->session->set('betIds', $betIds);

        return true;
    }

    /**
     * @return BetSlip
     */
    public function processBetsForBetSlip()
    {
        /** @var BetRepository $betRepository */
        $betRepository = $this->entityManager->getRepository(Bet::class);

        $betIds = [];
        if ($this->session->has('betIds')) {
            $betIds = $this->session->get('betIds');
        }

        $betSlip = new BetSlip();
        foreach ($betIds as $betId) {
            /** @var Bet $bet */
            $bet = $betRepository->find($betId);

            if ($bet instanceof Bet) {
                $betSlip->getBets()->add($bet);
            }
        }

        return $betSlip;
    }

    /**
     * @param $betId
     * @return bool
     */
    public function removeSessionVariable($betId)
    {
        if (!$this->session->has('betIds')) {
            return true;
        }

        $betIds = $this->session->get('betIds');

        foreach ($betIds as $key => $bet) {
            if ($bet == $betId) {
                unset($betIds[$key]);
                break;
            }
        }

        $this->session->set('betIds', $betIds);
        return true;
    }

    /**
     * @param $stake
     *
     * @return string
     */
    public function saveBetSlip($stake)
    {
        if (!$this->session->has('betIds')) {
            throw new Exception($this->translator->trans('bets.noBetsError'));
        }
        if (!$this->baseService->isUserLogged()) {
            throw new Exception($this->translator->trans('bets.userError'));
        }

        /** @var User $user */
        $user = $this->baseService->getUser();
        /** @var UserBudget $userBudget */
        $userBudget = $user->getUserBudget();

        if ($userBudget->getBudget() < $stake) {
            throw new Exception($this->translator->trans('bets.budgetError'));
        }

        $userBudget->setBudget($userBudget->getBudget() - $stake);

        $betIds = $this->session->get('betIds');

        /** @var BetRepository $betRepository */
        $betRepository = $this->entityManager->getRepository(Bet::class);

        $userTransaction = new UserTransaction();
        $userTransaction
            ->setUser($user)
            ->setAmount($stake)
            ->setDetails('New bet slip added')
            ->setStatus(UserTransaction::STATUS_ACTIVE)
            ->setUserBudget($userBudget)
            ->setType(UserTransaction::TYPE_STAKE);

        $betSlip = new BetSlip();
        $betSlip
            ->setUser($user)
            ->setStatus(BetSlip::STATUS_NEW)
            ->setUserTransaction($userTransaction)
            ->setStake($stake);

        $userTransaction->setBetSlip($betSlip);

        $odd = 1;
        foreach ($betIds as $betId) {
            /** @var Bet $bet */
            $bet = $betRepository->find($betId);
            $betSlip->getBets()->add($bet);
            $odd *= $bet->getOdd();
        }

        $betSlip->setIncome($odd * $stake);

        $this->entityManager->persist($betSlip);
        $this->entityManager->persist($userTransaction);
        $this->entityManager->flush();

        $this->session->remove('betIds');

        return $this->translator->trans('bets.success',['budget' => $userBudget->getBudget()]);
    }

    public function updateBetSlipsStatus()
    {
        /** @var BetSlipRepository $betSlipRepository */
        $betSlipRepository = $this->entityManager->getRepository(BetSlip::class);

        /** @var BetSlip[] $betSlips */
        $betSlips = $betSlipRepository->findBy([
            'status' => BetSlip::STATUS_NEW
        ]);

        foreach ($betSlips as $betSlip) {
            $bets = $betSlip->getBets();

            /** @var Bet $bet */
            foreach ($bets as $bet) {
                $gameDetails = $bet->getGame()->getGameDetails();

                if ($gameDetails->getStatus() == GameDetails::STATUS_FINISHED) {
                    if ($bet->getBetType()->getId() == BetType::TYPE_1) {
                        $betWon = $gameDetails->getHomeTeamScore() > $gameDetails->getAwayTeamScore();
                    } elseif ($bet->getBetType()->getId() == BetType::TYPE_X) {
                        $betWon = $gameDetails->getHomeTeamScore() == $gameDetails->getAwayTeamScore();
                    } else {
                        $betWon = $gameDetails->getHomeTeamScore() < $gameDetails->getAwayTeamScore();
                    }
                } else {
                    $betSlip->setStatus(BetSlip::STATUS_IN_PROGRESS);
                    break;
                }
                if (!$betWon) {
                    $betSlip->setStatus(BetSlip::STATUS_LOST);
                    break;
                }
            }

            if ($betSlip->getStatus() == BetSlip::STATUS_NEW) {
                $betSlip->setStatus(BetSlip::STATUS_WON);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ApiService $apiService
     */
    public function setApiService(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param BaseService $baseService
     */
    public function setBaseService(BaseService $baseService)
    {
        $this->baseService = $baseService;
    }
}