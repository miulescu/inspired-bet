<?php

namespace BetBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use GameBundle\Entity\Game;

/**
 * Bet
 *
 * @ORM\Table(name="bet")
 * @ORM\Entity(repositoryClass="BetBundle\Repository\BetRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Bet
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var BetType
     *
     * @ORM\ManyToOne(targetEntity="BetBundle\Entity\BetType", inversedBy="bets")
     * @ORM\JoinColumn(name="bet_type_id", referencedColumnName="id")
     */
    private $betType;

    /**
     * @var float
     *
     * @ORM\Column(name="odd", type="float")
     */
    private $odd;

    /**
     * @var Game
     *
     * @ORM\ManyToOne(targetEntity="GameBundle\Entity\Game", inversedBy="bets")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    private $game;

    /**
     * @var BetSlip[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="BetBundle\Entity\BetSlip", mappedBy="bets")
     */
    private $betSlips;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * Bet constructor.
     */
    public function __construct()
    {
        $this->betSlips = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return BetType
     */
    public function getBetType()
    {
        return $this->betType;
    }

    /**
     * @param $betType
     * @return Bet
     */
    public function setBetType($betType)
    {
        $this->betType = $betType;
        return $this;
    }

    /**
     * Set odd
     *
     * @param float $odd
     *
     * @return Bet
     */
    public function setOdd($odd)
    {
        $this->odd = $odd;

        return $this;
    }

    /**
     * Get odd
     *
     * @return float
     */
    public function getOdd()
    {
        return $this->odd;
    }

    /**
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param $game
     * @return Bet
     */
    public function setGame($game)
    {
        $this->game = $game;
        return $this;
    }

    /**
     * @return BetSlip[]|ArrayCollection
     */
    public function getBetSlips()
    {
        return $this->betSlips;
    }

    /**
     * @param BetSlip[]|ArrayCollection $betSlips
     * @return Bet
     */
    public function setBetSlips($betSlips)
    {
        $this->betSlips = $betSlips;
        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Bet
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return (string) self::getOdd();
    }
}

