<?php

namespace BetBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BetType
 *
 * @ORM\Table(name="bet_type")
 * @ORM\Entity(repositoryClass="BetBundle\Repository\BetTypeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class BetType
{
    use TimestampableTrait;

    /** @const int */
    const STATUS_ACTIVE = 1;

    /** @const int */
    const TYPE_1 = 1;

    /** @const int */
    const TYPE_X = 2;

    /** @const int */
    const TYPE_2 = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10)
     */
    private $code;

    /**
     * @var BetGroup
     *
     * @ORM\ManyToOne(targetEntity="BetBundle\Entity\BetGroup", inversedBy="betTypes")
     * @ORM\JoinColumn(name="bet_group_id", referencedColumnName="id")
     */
    private $betGroup;

    /**
     * @var Bet[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BetBundle\Entity\Bet", mappedBy="betType", cascade={"persist"})
     */
    private $bets;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * BetType constructor.
     */
    public function __construct()
    {
        $this->bets = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return BetType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return BetGroup
     */
    public function getBetGroup(): BetGroup
    {
        return $this->betGroup;
    }

    /**
     * @param $betGroup
     * @return BetType
     */
    public function setBetGroup($betGroup)
    {
        $this->betGroup = $betGroup;

        return $this;
    }

    /**
     * @return Bet[]|ArrayCollection
     */
    public function getBets()
    {
        return $this->bets;
    }

    /**
     * @param Bet[]|ArrayCollection $bets
     * @return BetType
     */
    public function setBets($bets)
    {
        $this->bets = $bets;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BetType
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    function __toString()
    {
        return self::getCode();
    }
}

