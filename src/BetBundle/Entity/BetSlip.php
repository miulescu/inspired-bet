<?php

namespace BetBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use BaseBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\UserTransaction;

/**
 * BetSlip
 *
 * @ORM\Table(name="bet_slip")
 * @ORM\Entity(repositoryClass="BetBundle\Repository\BetSlipRepository")
 * @ORM\HasLifecycleCallbacks
 */
class BetSlip
{
    use TimestampableTrait;

    const STATUS_NEW = 1;

    const STATUS_IN_PROGRESS = 2;

    const STATUS_CANCELED = 9;

    const STATUS_LOST = 3;

    const STATUS_WON = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User", inversedBy="betSlips")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="stake", type="float")
     */
    private $stake;

    /**
     * @var UserTransaction
     *
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\UserTransaction", inversedBy="betSlip")
     * @ORM\JoinColumn(name="user_transaction", referencedColumnName="id")
     */
    private $userTransaction;

    /**
     * @var float
     *
     * @ORM\Column(name="income", type="float")
     */
    private $income;

    /**
     * @var Bet[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="BetBundle\Entity\Bet", inversedBy="betSlips", cascade={"persist"})
     * @ORM\JoinTable(name="bet_slips_bets")
     */
    private $bets;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * BetSlip constructor.
     */
    public function __construct()
    {
        $this->bets = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return BetSlip
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return float
     */
    public function getStake()
    {
        return $this->stake;
    }

    /**
     * @param float $stake
     *
     * @return BetSlip
     */
    public function setStake(float $stake)
    {
        $this->stake = $stake;

        return $this;
    }

    /**
     * @return UserTransaction
     */
    public function getUserTransaction()
    {
        return $this->userTransaction;
    }

    /**
     * @param UserTransaction $userTransaction
     *
     * @return BetSlip
     */
    public function setUserTransaction(UserTransaction $userTransaction)
    {
        $this->userTransaction = $userTransaction;

        return $this;
    }

    /**
     * @return float
     */
    public function getIncome()
    {
        return $this->income;
    }

    /**
     * @param float $income
     *
     * @return BetSlip
     */
    public function setIncome($income)
    {
        $this->income = $income;

        return $this;
    }

    /**
     * @return Bet[]|ArrayCollection
     */
    public function getBets()
    {
        return $this->bets;
    }

    /**
     * @param Bet[]|ArrayCollection $bets
     * @return BetSlip
     */
    public function setBets($bets)
    {
        $this->bets = $bets;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return BetSlip
     */
    public function setStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }
}

