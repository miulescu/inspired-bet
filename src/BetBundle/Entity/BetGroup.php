<?php

namespace BetBundle\Entity;

use BaseBundle\Entity\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BetGroup
 *
 * @ORM\Table(name="bet_group")
 * @ORM\Entity(repositoryClass="BetBundle\Repository\BetGroupRepository")
 * @ORM\HasLifecycleCallbacks
 */
class BetGroup
{
    use TimestampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     */
    private $code;

    /**
     * @var BetType[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BetBundle\Entity\BetType", mappedBy="betGroup", cascade={"persist"})
     */
    private $betTypes;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * BetGroup constructor.
     */
    public function __construct()
    {
        $this->betTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return BetGroup
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return BetType[]|ArrayCollection
     */
    public function getBetTypes()
    {
        return $this->betTypes;
    }

    /**
     * @param BetType[]|ArrayCollection $betTypes
     * @return BetGroup
     */
    public function setBetTypes($betTypes)
    {
        $this->betTypes = $betTypes;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BetGroup
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}

