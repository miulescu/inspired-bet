// Render the betslip in a div located in a box in body block with id: betSlipBox
function betSlip() {
    var url = Routing.generate('bet_betSlipAjax', {'_locale': locale });

    $.ajax({
        type: "GET",
        url: url,
        success: function (response) {
            if (response.error === false) {
                $('#betSlipBox').html(response.content);
                $('#bet_slip_stake').val(localStorage.getItem('stake'));
                var potentialGain = localStorage.getItem('stake') * $('#finalOdd').html();
                $('#potentialGain').html(potentialGain.toFixed(2));
                $('#bet_slip_income').val(potentialGain.toFixed(2));
            } else {
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            addNotification(errorThrown, 'danger');
        }
    })
}

// Add a new bet in session for Bet Slip and reload the Bet Slip
function addNewBet(betId) {
    var url = Routing.generate('bet_betSlipAjax', {'_locale': locale});

    $.ajax({
        type: "POST",
        url: url,
        data: {"betId": betId},
        dataType: "json",
        success: function (response) {
            if (response.error === false) {
                $('#betSlipBox').html(response.content);
                $('#bet_slip_stake').val(localStorage.getItem('stake'));
                var potentialGain = localStorage.getItem('stake') * $('#finalOdd').html();
                $('#potentialGain').html(potentialGain.toFixed(2));
                $('#bet_slip_income').val(potentialGain.toFixed(2));
            } else {
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            addNotification(errorThrown, 'danger');
        }
    })
}

function calculatePotentialGain() {
        var potentialGain = $('#bet_slip_stake').val() * $('#finalOdd').html();
        $('#potentialGain').html(potentialGain.toFixed(2));
        $('#bet_slip_income').val(potentialGain.toFixed(2));
        localStorage.setItem('stake', $('#bet_slip_stake').val());
}

function removeSessionVariable(betId) {
    var url = Routing.generate('bet_removeSessionVariableAjax', {'_locale': locale});

    $.ajax({
        type: "POST",
        url: url,
        data: {"betId": betId},
        dataType: "json",
        success: function (response) {
            if (response.error === false) {
                betSlip();
            } else {
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            addNotification(errorThrown, 'danger');
        }
    })
}

function saveBetSlip() {
    var url = Routing.generate('bet_saveBetSlipAjax', {'_locale': locale});

    $.ajax({
        type: "POST",
        url: url,
        data: {"stake": localStorage.getItem('stake')},
        dataType: "json",
        success: function (response) {
            if (response.error === false) {
                addNotification(response.message, 'info');
                betSlip();
            } else {
                addNotification(response.message, 'danger');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            addNotification(errorThrown, 'danger');
        }
    })
}